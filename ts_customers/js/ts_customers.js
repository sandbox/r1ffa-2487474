(function () {
  Drupal.behaviors.ts_customers = {
    attach: function (context, settings) {
      ts_customers_filter();
      ts_customers_make_grid();
      var permission = Drupal.settings.ts_customers.permission;
      if(permission){
        ts_customers_show_admin_buttons();
        ts_customers_make_sortable();
        ts_customers_ajax_onepage();
        ts_customers_send_customer_to_page();
        ts_customers_ajax_crosspage();
      }
      if(!permission){
        ts_customers_hover();
      }
    }
  };
})(jQuery);

function ts_customers_filter() {
  jQuery('.industry-dropdown-toggler,.industry-dropdown-active').click(function(event){
    event.preventDefault();
    jQuery(this).parent().toggleClass('closed');
  });
}

function ts_customers_make_grid() {
  var height = jQuery('.customers-item').width();
  jQuery('.customers-item').css({'height':height+'px'});
}

function ts_customers_show_admin_buttons() {
  jQuery('.show-admin-buttons').click(function(event){
    event.preventDefault();
    jQuery('#customers-grid').toggleClass('no-edit');
    jQuery('#ts-customers-priority-form button').toggleClass('no-edit');
  });
}

function ts_customers_make_sortable(){
  jQuery( "#customers-grid" ).sortable({});
  jQuery( "#customers-grid" ).disableSelection();

  return false;
}

function ts_customers_ajax_onepage(){
  var offset = Drupal.settings.ts_customers.offset;
        
  jQuery( "#ts-customers-priority-form" ).submit(function(){
    var changes = ts_customers_get_changes();
    var default_priority = jQuery( "#edit-priority-string" ).val();
    var priority_id = jQuery( "#edit-priority-id" ).val();
    var tid_branch = jQuery( "#edit-tid-branch" ).val();
    var tid_subbranch = jQuery( "#edit-tid-subbranch" ).val();
    var tid_product = jQuery( "#edit-tid-product" ).val();

    jQuery.ajax({
      url: '/ajax/tscustomers/onepage',
      data: {offset:offset, changes:changes, default_priority:default_priority, priority_id:priority_id, tid_branch:tid_branch, tid_subbranch:tid_subbranch, tid_product:tid_product},
      type: 'post',
      success: function(){
        location.reload();
      }
    });

    location.reload();

    return false;
  });
}

function ts_customers_get_changes() {
  var changes = '';

  jQuery('.customers-item').each(function(){
    var nid = jQuery(this).attr('nid');
    var weigth = jQuery(this).attr('weigth');
    var key_value = weigth+'-'+nid+'/';
    changes += key_value;
  });

  return changes;
}

function ts_customers_send_customer_to_page() {
  jQuery('.customer-to-page-toggler').click(function(){
    jQuery(this).parent().toggleClass('closed');
  });
}

function ts_customers_ajax_crosspage(){
  jQuery( ".customer-to-page-button" ).click(function(){
    var nid = jQuery(this).attr('nid');
    var to_page = jQuery(this).parent().children('.customer-to-page-input').val();
    if(to_page != ''){
      var default_priority = jQuery( "#edit-priority-string" ).val();
      var priority_id = jQuery( "#edit-priority-id" ).val();
      var tid_branch = jQuery( "#edit-tid-branch" ).val();
      var tid_subbranch = jQuery( "#edit-tid-subbranch" ).val();
      var tid_product = jQuery( "#edit-tid-product" ).val();
      var url = window.location.href.split('?')[0];

      jQuery.ajax({
        url: '/ajax/tscustomers/crosspage',
        data: {url:url, nid:nid, to_page:to_page, default_priority:default_priority, priority_id:priority_id, tid_branch:tid_branch, tid_subbranch:tid_subbranch, tid_product:tid_product},
        type: 'post',
        success: function(data){
          location.reload();
        }
      });
    }
    
    return false;
  });
}

function ts_customers_hover() {
  jQuery('.customers-item').hover(function(e){
    var id = jQuery(this).attr('id');
    jQuery('#'+id+' .customers_hover_box').animate({
      opacity: 1,
      width: "100%",
      left:"0"
    },250, function() {
      jQuery(this).children().css({"opacity":"1"});
    });
  },function(){
    var id = jQuery(this).attr('id');
    jQuery('#'+id+' .customers_hover_box').children().css({"opacity":"0"});
    jQuery('#'+id+' .customers_hover_box').stop().animate({
      opacity: 0,
      width: "0px",
      left:"50%"
    },250);
  });
}