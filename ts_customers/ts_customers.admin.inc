<?php
// $Id: ts_customers.admin.inc Exp $

function ts_customers_edit() {
  $form = array();

  $form['customers_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Customers per page'),
    '#default_value' => variable_get('customers_per_page', 12),
    '#maxlength' => 64,
  );
  
  $form['customers_per_row'] = array(
    '#type' => 'textfield',
    '#title' => t('Customers per row'),
    '#default_value' => variable_get('customers_per_row', 3),
    '#maxlength' => 64,
  );
  $form['customers_header'] = array(
    '#type' => 'textarea',
    '#title' => t('Customers header'),
    '#default_value' => variable_get('customers_header', ''),
  );
  $form['customers_footer'] = array(
    '#type' => 'textarea',
    '#title' => t('Customers footer'),
    '#default_value' => variable_get('customers_footer', ''),
  );

  return system_settings_form($form);
}