<?php
/**
 * @file
 * Page callbacks for ts_customers module.
 */

function theme_ts_customers_admin_view($variables) {
  $permission = $variables['permission'];
  $id_list = $variables['id_list'];
  $total_count = count($id_list);
  $customers_per_page = $variables['elements_per_page'];
  $customers_per_row = $variables['elements_per_row'];
  $offset = $variables['offset'];
  $total_pages = ceil($total_count/$customers_per_page);
  $end_item = $customers_per_page*$offset;

  if($end_item > $customers_per_page){
    $start_item = $end_item-$customers_per_page;
  }
  else{
    $start_item = 0;
  }
  $items_counter = 0;

  $output_item = '';

  drupal_add_css(drupal_get_path('module', 'ts_customers') . '/css/ts_customers.css');
  drupal_add_library('system', 'ui.sortable');
  drupal_add_js(array('ts_customers' => array('permission' => $permission, 'customers_per_page' => $customers_per_page, 'offset' => $offset)), array('type' => 'setting'));
  drupal_add_js(drupal_get_path('module', 'ts_customers') . '/js/ts_customers.js');

  $bootstrap_grid = 12;
  if(($customers_per_row % 2) == 0){
    $bootstrap_col = $bootstrap_grid/$customers_per_row;
  }
  else{
    $bootstrap_col = $bootstrap_grid%$customers_per_row;
  }
  $output_item .= '<div id="customers-control-panel">';
  $output_item .=  '<div class="cutomers_total"><strong>'.t('Cutomers total:').'</strong> '.$total_count.'</div>';
  $output_item .=  '<div class="cutomers_total"><strong>'.t('Cutomers per page:').'</strong> '.$customers_per_page.'</div>';
  $output_item .=  '<div class="cutomers_total"><strong>'.t('Pages count:').'</strong> '.$total_pages.'</div>';
  $output_item .= l(t('Show edit buttons'), '', array('attributes' => array('class' => 'show-admin-buttons')));
  $output_item .= '</div>';
  $output_item .= '<div id="customers-grid" class="no-edit">';

  foreach ($id_list as $key => $value) {
    if($items_counter>=$start_item){
      if($items_counter==$end_item){
        break;
      }
      $customer = node_load($value);
      if(isset($customer->field_customers_logo['und']['0']['uri'])){
        $customer_logo = '<img src="'.file_create_url($customer->field_customers_logo['und']['0']['uri']).'"/>';
      }
      else{
        $customer_logo = '<span class="customers-text-logo">'.$customer->title.'</span>';
      }
      if(isset($customer->field_customers_city['und']['0']['value'])){
        $cutomer_city = $customer->field_customers_city['und']['0']['value'];
      }
      else{
         $cutomer_city = '';
      }
      if(isset($customer->field_customers_site['und']['0']['value'])){
        $customer_title = l($customer->title, $customer->field_customers_site['und']['0']['value'], array('attributes' => array('target' => '_blank'), 'html' => TRUE,));
      }
      else{
        $customer_title = $customer->title;
      }
      if(isset($customer->field_customers_project['und']['0']['value'])){
        $customer_project = '<div class="project">'.l(t('Project description'), 'http://www.community.terrasoft.ru/pm/solutions/'.$customer->field_customers_project['und']['0']['value'], array('attributes' => array('target' => '_blank'), 'html' => TRUE,)).'</div>';
      }
      else{
        $customer_project = '';
      }
      if(isset($customer->field_customers_reference['und']['0']['uri'])){
        $customer_reference = '<div class="recomendation">'.l(t('Recommendations'), file_create_url($customer->field_customers_reference['und']['0']['uri']), array('attributes' => array('target' => '_blank'), 'html' => TRUE,)).'</div>';
      }
      else{
        $customer_reference = '';
      }
      if(isset($customer->field_customers_news['und']['0']['nid'])){
        $customer_news =  '<div class="to_news">'.l(t('News'), 'company/news/'.$customer->field_customers_news['und']['0']['nid']).'</div>';
      }
      else{
        $customer_news = '';
      }
      if(isset($customer->field_customers_review['und']['0']['nid'])){
        $customer_video =  '<div class="video">'.l(t('Video review'), 'customers/reviews#'.$customer->field_customers_review['und']['0']['nid']).'</div>';
      }
      else{
        $customer_video = '';
      }
      $output_item .= '
      <div id="customer-'.$value.'" nid="'.$value.'" weigth="'.$key.'" class="customers-item col-md-'.$bootstrap_col.' sortable" >
        <div class="customers-border">
          <div class="customer-logo-wrapper">
            <div class="customer-logo-baseline">'.$customer_logo.'</div>
          </div>
          <div class="customer-to-page-container closed">
            '.l(t('Edit'), 'node/'.$value.'/edit', array('attributes' => array('class' => 'customer-edit'))).'
            <input type="text" nid="'.$value.'" class="customer-to-page-input" placeholder="№">
            <button nid="'.$value.'" class="customer-to-page-button" >'.t('Page').'</button>
            <div class="customers_links admin">
                '.$customer_project.'
                '.$customer_reference.'
                '.$customer_news.'
                '.$customer_video.'      
             </div>
            <span class="customer-to-page-toggler"></span>
          </div>
          <div class="customer-city">'.$cutomer_city.'</div>
        </div>
      </div>';
      $items_counter++;
    }
    else{
      $items_counter++;
    }
  }

  $output_item .= '</div>';
  $output_item .= '<div class="clear"></div>';
  $output = $output_item;

  unset($output_item);
  unset($id_list);
  unset($customer);

  return $output;
}

function theme_ts_customers_user_view($variables) {
  $permission = $variables['permission'];
  $id_list = $variables['id_list'];
  $output_item = '';
  $customers_per_page = $variables['elements_per_page'];
  $customers_per_row = $variables['elements_per_row'];
  $offset = $variables['offset'];
  $end_item = $customers_per_page*$offset;

  if($end_item > $customers_per_page){
    $start_item = $end_item-$customers_per_page;
  }
  else{
    $start_item = 0;
  }
  $items_counter = 0;

  drupal_add_css(drupal_get_path('module', 'ts_customers') . '/css/ts_customers.css');
  drupal_add_js(array('ts_customers' => array('permission' => $permission)), array('type' => 'setting'));
  drupal_add_js(drupal_get_path('module', 'ts_customers') . '/js/ts_customers.js');

  $bootstrap_grid = 12;
  if(($customers_per_row % 2) == 0){
    $bootstrap_col = $bootstrap_grid/$customers_per_row;
  }
  else{
    $bootstrap_col = $bootstrap_grid%$customers_per_row;
  }

  $output_item .= '<div id="customers-grid">';

  foreach ($id_list as $key => $value) {
    if($items_counter>=$start_item){
      if($items_counter==$end_item){
        break;
      }

      $customer = node_load($value);
      //visible container
      if(isset($customer->field_customers_logo['und']['0']['uri'])){
        $customer_logo = '<img src="'.file_create_url($customer->field_customers_logo['und']['0']['uri']).'"/>';
      }
      else{
        $customer_logo = '<span class="customers-text-logo">'.$customer->title.'</span>';
      }
      if(isset($customer->field_customers_city['und']['0']['value'])){
        $cutomer_city = $customer->field_customers_city['und']['0']['value'];
      }
      else{
        $cutomer_city = '';
      }
      //hover container
      if(isset($customer->field_customers_site['und']['0']['value'])){
        $customer_title = l($customer->title, $customer->field_customers_site['und']['0']['value'], array('attributes' => array('target' => '_blank'), 'html' => TRUE,));
      }
      else{
        $customer_title = $customer->title;
      }
      if(isset($customer->field_customers_project['und']['0']['value'])){
        $customer_project = '<div class="project">'.l(t('Project description'), 'http://www.community.terrasoft.ru/pm/solutions/'.$customer->field_customers_project['und']['0']['value'], array('attributes' => array('target' => '_blank'), 'html' => TRUE,)).'</div>';
      }
      else{
        $customer_project = '';
      }
      if(isset($customer->field_customers_reference['und']['0']['uri'])){
        $customer_reference = '<div class="recomendation">'.l(t('Recommendations'), file_create_url($customer->field_customers_reference['und']['0']['uri']), array('attributes' => array('target' => '_blank'), 'html' => TRUE,)).'</div>';
      }
      else{
        $customer_reference = '';
      }
      if(isset($customer->field_customers_news['und']['0']['nid'])){
        $customer_news =  '<div class="to_news">'.l(t('News'), 'company/news/'.$customer->field_customers_news['und']['0']['nid']).'</div>';
      }
      else{
        $customer_news = '';
      }
      if(isset($customer->field_customers_review['und']['0']['nid'])){
        $customer_video =  '<div class="video">'.l(t('Video review'), 'customers/reviews#'.$customer->field_customers_review['und']['0']['nid']).'</div>';
      }
      else{
        $customer_video = '';
      }
      //concat hover content
      if((strlen($customer_project)>1)||(strlen($customer_reference)>1)||(strlen($customer_news)>1)||(strlen($customer_video)>1)){
        $hover_container = 
        '<div class="customers_hover_box">
            <span class="customers_title">'.$customer_title.'</span>
             <div class="customers_links">
                '.$customer_project.'
                '.$customer_reference.'
                '.$customer_news.'
                '.$customer_video.'      
             </div>
        </div>';
      }
      else{
        $hover_container = '';
      }

      $output_item .= '
      <div class="customers-item col-md-'.$bootstrap_col.'" id="node-'.$customer->nid.'">
        <div class="customers-border">
          <div class="customer-logo-wrapper">
            <div class="customer-logo-baseline">'.$customer_logo.'</div>
          </div>
          <div class="customer-city">'.$cutomer_city.'</div>
          '.$hover_container.'
        </div>
      </div>';

      $items_counter++;
    }
    else{
      $items_counter++;
    }
  }

  $output_item .= '</div>';
  $output_item .= '<div class="clear"></div>';
  $output = $output_item;

  unset($output_item);
  unset($id_list);
  unset($customer);

  return $output;
}

function theme_ts_customers_pager($variables) {
  $pages_count = $variables['pages_count'];
  $visible_items = $variables['visible_items'];
  $current_page = $variables['current_page'];
  $current_url_array = $_SERVER['REQUEST_URI'];
  $current_url_array = explode("?", $_SERVER['REQUEST_URI']);
  $current_url = $current_url_array[0];
  $previous_page_sign = '<';
  $next_page_sign = '>';

  if($pages_count<=1){
    $output = '';
    return $output;
  }

  if($visible_items>$pages_count){
    $visible_items = $pages_count;
  }
  if($current_page > $visible_items){
    $start_page = $current_page-(floor($visible_items/2));
    $end_page = $current_page+(floor($visible_items/2));
    if($end_page > $pages_count){
      $end_page = $pages_count;
    }
  }
  else{
    if($current_page > floor($visible_items/2)){
      $start_page = $current_page-(floor($visible_items/2));
      $end_page = $current_page+(floor($visible_items/2));
    }
    else{
      $start_page = 1;
      $end_page = $visible_items;
    }
  }

  $output_item = '<ul class="pager" >';

  if($current_page!=1){
    $output_item .='<li class="pager-item pager-previous" ><a href="'.$current_url.'?page=1">'.$previous_page_sign.'</a></li>';
  }

  for ($i = $start_page; $i < $end_page+1; $i++) {
    if($current_page==$i){
      $output_item .='<li class="pager-item pager-current"><span class="current-page">'.$i.'</span></li>';
    }
    else{
      $output_item .='<li class="pager-item"><a href="'.$current_url.'?page='.$i.'">'.$i.'</a></li>';
    }
  }

  if($current_page!=$pages_count){
    $output_item .='<li class="pager-item pager-next" ><a href="'.$current_url.'?page=1">'.$next_page_sign.'</a></li>';
  }

  $output_item .= '</ul>';

  $output = $output_item;

  unset($current_url_array);
  unset($output_item);

  return $output;
}